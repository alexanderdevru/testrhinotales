using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawPoints : MonoBehaviour
{
    [SerializeField]
    private Transform[] _parentsColliders;
    [SerializeField]
    private int[] _resolution;

    // Start is called before the first frame update
    void Start()
    {
        var colliders = FindObjectsOfType<Collider>();
        foreach (var collider in colliders)
        {
            collider.isTrigger = true;
        }
        foreach (Transform parentColliders in _parentsColliders)
        {
            int collidersCount = parentColliders.childCount;
            for (int i = 0; i < collidersCount; i++)
            {
                parentColliders.GetChild(i).GetComponent<BoxCollider>().isTrigger = true;
                parentColliders.GetChild(i).GetComponent<BoxCollider>().enabled = true;
                Bounds bounds = parentColliders.GetChild(i).GetComponent<BoxCollider>().bounds;
                var stepZ = bounds.size.z / _resolution[2];
                var stepY = bounds.size.y / _resolution[1];
                var stepX = bounds.size.x / _resolution[0];
                for (int j = 0; j < _resolution[0]; j++)
                {
                    for (int k = 0; k < _resolution[1]; k++)
                    {
                        for (int l = 0; l < _resolution[2]; l++)
                        {
                            //������� ����� �� ���������
                            var sphere01 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                            sphere01.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
                            sphere01.transform.position = new Vector3(bounds.min.x + j * stepX, bounds.min.y + k * stepY, bounds.min.z + l * stepZ);
                            sphere01.transform.SetParent(parentColliders.GetChild(i));
                        }
                    }
                }
            }
        }
    }
}
